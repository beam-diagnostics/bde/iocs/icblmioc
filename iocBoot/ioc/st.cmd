< envPaths

errlogInit(20000)

dbLoadDatabase("$(PICO8APP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("PICODEV",        "/dev/amc_pico_0000:08:00.0")

# P macro for all records
epicsEnvSet("PREFIX",         "ICBLM:")
# R macro for all records
epicsEnvSet("ACQ_UNIT",       "AMC1")
# The port name for the detector
epicsEnvSet("PORT",           "PICO8")

epicsEnvSet("QSIZE",          "20")
epicsEnvSet("XSIZE",          "1")
epicsEnvSet("YSIZE",          "1000000")
epicsEnvSet("NCHANS",         "2048")
epicsEnvSet("TSPOINTS",       "2048")
epicsEnvSet("CBUFFS",         "500")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db:$(ADPICO8)/db:$(AUTOSAVE)/db")

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "10000000")

# Create a pico8 driver
# pico8Configure(const char *portName, const char *devicePath,
#             int maxAOI, int numSamples, int dataType,
#             int maxBuffers, int maxMemory,
#             int priority, int stackSize)
#dataType Initial data type of the detector data. These are the enum values for NDDataType_t, i.e.
#    0=NDInt8
#    1=NDUInt8
#    2=NDInt16
#    3=NDUInt16
#    4=NDInt32
#    5=NDUInt32
#    6=NDFloat32
#    7=NDFloat64
pico8Configure("$(PORT)", "$(PICODEV)", 0, $(YSIZE), 6, 0, 0)
dbLoadRecords("pico8.template",        "P=$(PREFIX),R=$(ACQ_UNIT):,   PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(YSIZE)")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):1:, PORT=$(PORT),ADDR=0,TIMEOUT=1,NAME=CH1")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):2:, PORT=$(PORT),ADDR=1,TIMEOUT=1,NAME=CH2")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):3:, PORT=$(PORT),ADDR=2,TIMEOUT=1,NAME=CH3")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):4:, PORT=$(PORT),ADDR=3,TIMEOUT=1,NAME=CH4")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):5:, PORT=$(PORT),ADDR=4,TIMEOUT=1,NAME=CH5")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):6:, PORT=$(PORT),ADDR=5,TIMEOUT=1,NAME=CH6")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):7:, PORT=$(PORT),ADDR=6,TIMEOUT=1,NAME=CH7")
dbLoadRecords("pico8N.template",       "P=$(PREFIX),R=$(ACQ_UNIT):8:, PORT=$(PORT),ADDR=7,TIMEOUT=1,NAME=CH8")
# expose channels to clients
NDStdArraysConfigure("$(PORT).AI1", 3, 0, "$(PORT)", 0)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI1:, PORT=$(PORT).AI1,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
NDStdArraysConfigure("$(PORT).AI2", 3, 0, "$(PORT)", 1)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI2:, PORT=$(PORT).AI2,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=1,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
NDStdArraysConfigure("$(PORT).AI3", 3, 0, "$(PORT)", 2)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI3:, PORT=$(PORT).AI3,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=2,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
NDStdArraysConfigure("$(PORT).AI4", 3, 0, "$(PORT)", 3)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI4:, PORT=$(PORT).AI4,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=3,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
NDStdArraysConfigure("$(PORT).AI5", 3, 0, "$(PORT)", 4)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI5:, PORT=$(PORT).AI5,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=4,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
NDStdArraysConfigure("$(PORT).AI6", 3, 0, "$(PORT)", 5)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI6:, PORT=$(PORT).AI6,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=5,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
NDStdArraysConfigure("$(PORT).AI7", 3, 0, "$(PORT)", 6)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI7:, PORT=$(PORT).AI7,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=6,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
NDStdArraysConfigure("$(PORT).AI8", 3, 0, "$(PORT)", 7)
dbLoadRecords("NDStdArrays.template",  "P=$(PREFIX),R=$(ACQ_UNIT):AI8:, PORT=$(PORT).AI8,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=7,TYPE=Float32,FTVL=FLOAT,NELEMENTS=$(YSIZE)")
# add ROI for each channel
NDROIConfigure("$(PORT).ROI1", $(QSIZE), 0, "$(PORT)", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI1:, PORT=$(PORT).ROI1, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=0")
NDROIConfigure("$(PORT).ROI2", $(QSIZE), 0, "$(PORT)", 1, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI2:, PORT=$(PORT).ROI2, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=1")
NDROIConfigure("$(PORT).ROI3", $(QSIZE), 0, "$(PORT)", 2, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI3:, PORT=$(PORT).ROI3, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=2")
NDROIConfigure("$(PORT).ROI4", $(QSIZE), 0, "$(PORT)", 3, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI4:, PORT=$(PORT).ROI4, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=3")
NDROIConfigure("$(PORT).ROI5", $(QSIZE), 0, "$(PORT)", 4, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI5:, PORT=$(PORT).ROI5, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=4")
NDROIConfigure("$(PORT).ROI6", $(QSIZE), 0, "$(PORT)", 5, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI6:, PORT=$(PORT).ROI6, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=5")
NDROIConfigure("$(PORT).ROI7", $(QSIZE), 0, "$(PORT)", 6, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI7:, PORT=$(PORT).ROI7, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=6")
NDROIConfigure("$(PORT).ROI8", $(QSIZE), 0, "$(PORT)", 7, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDROI.template",        "P=$(PREFIX),R=$(ACQ_UNIT):ROI8:, PORT=$(PORT).ROI8, ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=7")

# add statistics for each channel
NDStatsConfigure("$(PORT).ST1", $(QSIZE), 0, "$(PORT).ROI1", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST1:,  PORT=$(PORT).ST1,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI1,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST1_TS")
NDTimeSeriesConfigure("$(PORT).ST1_TS", $(QSIZE), 0, "$(PORT).ST1", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST1:TS:, PORT=$(PORT).ST1_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST1,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

NDStatsConfigure("$(PORT).ST2", $(QSIZE), 0, "$(PORT).ROI2", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST2:,  PORT=$(PORT).ST2,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI2,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST2_TS")
NDTimeSeriesConfigure("$(PORT).ST2_TS", $(QSIZE), 0, "$(PORT).ST2", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST2:TS:, PORT=$(PORT).ST2_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST2,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

NDStatsConfigure("$(PORT).ST3", $(QSIZE), 0, "$(PORT).ROI3", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST3:,  PORT=$(PORT).ST3,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI3,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST3_TS")
NDTimeSeriesConfigure("$(PORT).ST3_TS", $(QSIZE), 0, "$(PORT).ST3", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST3:TS:, PORT=$(PORT).ST3_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST3,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

NDStatsConfigure("$(PORT).ST4", $(QSIZE), 0, "$(PORT).ROI4", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST4:,  PORT=$(PORT).ST4,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI4,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST4_TS")
NDTimeSeriesConfigure("$(PORT).ST4_TS", $(QSIZE), 0, "$(PORT).ST4", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST4:TS:, PORT=$(PORT).ST4_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST4,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

NDStatsConfigure("$(PORT).ST5", $(QSIZE), 0, "$(PORT).ROI5", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST5:,  PORT=$(PORT).ST5,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI5,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST5_TS")
NDTimeSeriesConfigure("$(PORT).ST5_TS", $(QSIZE), 0, "$(PORT).ST5", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST5:TS:, PORT=$(PORT).ST5_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST5,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

NDStatsConfigure("$(PORT).ST6", $(QSIZE), 0, "$(PORT).ROI6", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST6:,  PORT=$(PORT).ST6,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI6,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST6_TS")
NDTimeSeriesConfigure("$(PORT).ST6_TS", $(QSIZE), 0, "$(PORT).ST6", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST6:TS:, PORT=$(PORT).ST6_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST6,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

NDStatsConfigure("$(PORT).ST7", $(QSIZE), 0, "$(PORT).ROI7", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST7:,  PORT=$(PORT).ST7,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI7,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST7_TS")
NDTimeSeriesConfigure("$(PORT).ST7_TS", $(QSIZE), 0, "$(PORT).ST7", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST7:TS:, PORT=$(PORT).ST7_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST7,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

NDStatsConfigure("$(PORT).ST8", $(QSIZE), 0, "$(PORT).ROI8", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
dbLoadRecords("NDStats.template",      "P=$(PREFIX),R=$(ACQ_UNIT):ST8:,  PORT=$(PORT).ST8,  ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ROI8,NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS),TS_PORT=$(PORT).ST8_TS")
NDTimeSeriesConfigure("$(PORT).ST8_TS", $(QSIZE), 0, "$(PORT).ST8", 1, 23)
dbLoadRecords("NDTimeSeries.template", "P=$(PREFIX),R=$(ACQ_UNIT):ST8:TS:, PORT=$(PORT).ST8_TS,ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(PORT).ST8,NDARRAY_ADDR=1,NCHANS=$(NCHANS),ENABLED=1")

set_requestfile_path("./")
set_requestfile_path("$(ADPICO8)/req")
set_requestfile_path("$(ADCORE)/req")
set_requestfile_path("$(BUSY)/req")
set_requestfile_path("$(SSCAN)/req")
set_requestfile_path("$(CALC)/req")
set_savefile_path("../autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")
save_restoreSet_status_prefix("$(PREFIX)")
dbLoadRecords("save_restoreStatus.db", "P=$(PREFIX)")

# asynSetTraceIOMask("$(PORT)",0,2)
# asynSetTraceMask("$(PORT)",0,255)

iocInit()

# save things every thirty seconds
create_monitor_set("auto_settings.req", 30, "P=$(PREFIX), R=$(ACQ_UNIT):")
